import { render, screen } from "@testing-library/react";
import App from "./App";

test("renders Contacts App title", () => {
  render(<App />);
  const linkElement = screen.getByText(/Contacts App/i);
  expect(linkElement).toBeInTheDocument();
});
