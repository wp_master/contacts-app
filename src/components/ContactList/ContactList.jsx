import React from "react";
import {
  ContactDetailsWrapper,
  SingleContact,
  SingleContactItem,
} from "./styles";
import { Link } from "react-router-dom";

const ContactList = ({ contactlistdata }) => {
  return (
    <>
      <ContactDetailsWrapper>
        {contactlistdata?.map((contact) => (
          <SingleContact key={contact.cell}>
            <Link
              to={`/contact-details/${contact.login.uuid}`}
              contactlistdata={contactlistdata}
            >
              <SingleContactItem key={contact.id.value}>
                <img src={contact.picture.large} alt="" />
              </SingleContactItem>
              <SingleContactItem>{`${contact.name.title} ${contact.name.first} ${contact.name.last}`}</SingleContactItem>
            </Link>
          </SingleContact>
        ))}
      </ContactDetailsWrapper>
    </>
  );
};

export default ContactList;
