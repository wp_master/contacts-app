import styled from "styled-components";

const size = {
  mobileS: "320px",
  mobileM: "375px",
  mobileL: "425px",
  tablet: "768px",
  laptop: "1024px",
  laptopL: "1440px",
  desktop: "2560px",
};

const device = {
  mobileS: `(min-width: ${size.mobileS})`,
  mobileM: `(min-width: ${size.mobileM})`,
  mobileL: `(min-width: ${size.mobileL})`,
  tablet: `(min-width: ${size.tablet})`,
  laptop: `(min-width: ${size.laptop})`,
  laptopL: `(min-width: ${size.laptopL})`,
  desktop: `(min-width: ${size.desktop})`,
  desktopL: `(min-width: ${size.desktop})`,
};

const ContactDetailsWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  font-size: 2.2rem;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  width: 30%;
  padding: 20px;

  @media ${device.laptop} {
    width: 30% !important;
  }

  @media ${device.mobileM} {
    width: 100%;
  }
`;

const SingleContact = styled.div`
  display: flex;
  width: 20rem;
  flex-grow: 1;
  justify-content: center;
  background: #efeeee;
  border-radius: 10px;
  -webkit-box-shadow: 1px 0px 14px 0px rgba(0, 0, 0, 0.3);
  box-shadow: 1px 0px 14px 0px rgba(0, 0, 0, 0.3);
  padding: 10px;
  font-size: 1.4rem;
  flex: 40%;
  margin: 10px;
  cursor: pointer;

  @media ${device.mobileM} {
    flex: 30%;
  }
`;

const SingleContactItem = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  justify-content: center;
  color: black;
  text-decoration: none;
  outline: none;

  @media ${device.mobileM} {
    font-size: 0.8rem;
    margin-top: 0.5rem;
  }
`;

export { ContactDetailsWrapper, SingleContact, SingleContactItem };
