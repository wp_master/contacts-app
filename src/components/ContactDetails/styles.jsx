import styled from "styled-components";

const size = {
  mobileS: "320px",
  mobileM: "375px",
  mobileL: "425px",
  tablet: "768px",
  laptop: "1024px",
  laptopL: "1440px",
  desktop: "2560px",
};

const device = {
  mobileS: `(min-width: ${size.mobileS})`,
  mobileM: `(min-width: ${size.mobileM})`,
  mobileL: `(min-width: ${size.mobileL})`,
  tablet: `(min-width: ${size.tablet})`,
  laptop: `(min-width: ${size.laptop})`,
  laptopL: `(min-width: ${size.laptopL})`,
  desktop: `(min-width: ${size.desktop})`,
  desktopL: `(min-width: ${size.desktop})`,
};

const ContactDetailsContainer = styled.div`
  justify-content: flex-end;
  display: flex;
  border: 1px solid gray;
  -webkit-box-shadow: 1px 0px 14px 0px rgba(0, 0, 0, 0.3);
  box-shadow: 1px 0px 14px 0px rgba(0, 0, 0, 0.3);
  border-radius: 10px;
  padding: 20px;
  margin: 20px;
  flex: 80%;

  > .MuiSvgIcon-root {
    color: gray;
    font-size: 2rem;
    cursor: pointer;
  }
`;

const ContactDeatailsInfo = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  flex-grow: 1;
`;

const DetailsRowsContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const ContactDetailsRow = styled.span`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 0.6rem;
`;

const ContactDetailsImg = styled.div`
  > img {
    border-radius: 10px;
  }
`;

export {
  ContactDetailsContainer,
  ContactDeatailsInfo,
  DetailsRowsContainer,
  ContactDetailsRow,
  ContactDetailsImg,
};
