import React from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  ContactDeatailsInfo,
  ContactDetailsContainer,
  ContactDetailsImg,
  ContactDetailsRow,
  DetailsRowsContainer,
} from "./styles";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";

const ContactDetails = ({ contactlistdata }) => {
  //get ID parameter from URL
  let { contactId } = useParams();
  //navigation hook
  const navigate = useNavigate();

  //filter data and create array of specofic contact by id
  const contactInfo = contactlistdata.filter((contact) => {
    return contact.login.uuid === contactId;
  });

  return (
    <ContactDetailsContainer>
      <ArrowBackIosIcon onClick={() => navigate(-1)}> Back </ArrowBackIosIcon>
      <>
        {contactInfo.map((contactItem) => (
          <ContactDeatailsInfo key={contactItem.login.uuid}>
            <ContactDetailsImg>
              <img src={contactItem.picture.large} alt="" />
            </ContactDetailsImg>
            <DetailsRowsContainer>
              <ContactDetailsRow>{`${contactItem.name.title} ${contactItem.name.first} ${contactItem.name.last}`}</ContactDetailsRow>
              <ContactDetailsRow>{`${contactItem.location.street.number} ${contactItem.location.street.name} St , ${contactItem.location.city}, ${contactItem.location.country}`}</ContactDetailsRow>
              <ContactDetailsRow>{`Email- ${contactItem.email} `}</ContactDetailsRow>
              <ContactDetailsRow>{` ZipCode- ${contactItem.location.postcode}`}</ContactDetailsRow>
            </DetailsRowsContainer>
          </ContactDeatailsInfo>
        ))}
      </>
    </ContactDetailsContainer>
  );
};

export default ContactDetails;
