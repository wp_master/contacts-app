import "./App.css";
import { Routes, Route } from "react-router-dom";
import ContactList from "./components/ContactList/ContactList";
import ContactDetails from "./components/ContactDetails/ContactDetails";
import { useEffect, useState } from "react";
import axios from "axios";

function App() {
  const [ContactListData, setContactListData] = useState("");
  //get contact list data
  const fetchData = async () => {
    const request = axios({
      method: "get",
      url: "https://randomuser.me/api?results=10",
    });

    const result = await request;

    const ContactsData = result?.data?.results;
    //set state with contacts data
    setContactListData(ContactsData);
  };

  //load fetchData function
  useEffect(() => {
    fetchData();
  }, []);
  return (
    <div className="App">
      <Routes>
        <Route
          path="/"
          exact
          element={
            ContactListData && <ContactList contactlistdata={ContactListData} />
          }
        />
        <Route
          path="/contact-details/:contactId"
          exact
          element={<ContactDetails contactlistdata={ContactListData} />}
        />
      </Routes>
    </div>
  );
}

export default App;
